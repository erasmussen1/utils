#!/usr/bin/env python3
#
#
import json
import sys

try:
    input_str = sys.stdin.read()

    print(json.dumps(json.loads(input_str), sort_keys=True, indent=4))

except IOError:
    print("ERR: Couldn't decode \n {} \n".format(input_str))
