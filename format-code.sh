#!/bin/bash
#
# Description:
#   Formats c, c++, h files
#
#   At this point the script only accepts c, cpp, h files and
#   the clang-format configurations are for 3.9 or later
#
# Nice nice little web tool to try clang-format options:
#   http://zed0.co.uk/clang-format-configurator/
#
# Here one other one:
#   https://clang-format-configurator.site/
#

set -o nounset
set -o errexit
# set -x

SCRIPT_VERSION="1.0.0.15"

SCRIPT_DIR="$(cd $(dirname $0) && pwd)"

CLANG_FORMAT=clang-format
CLANG_VERSION=$(clang-format --version | sed -r 's/.* ([0-9]*\.[0-9]*)..*/\1/g')

DEFAULT_CLANG_FORMAT="${SCRIPT_DIR}/clang_format_18.0"

die()
{
    echo "$*" && exit 1
}

format_files_in_directory()
{
    if [[ $# -lt 2 ]]; then
        return
    fi

    local recursive="$1"
    shift
    local directory=$*

    recursive_flag=""
    if [[ $recursive -eq 0 ]]; then
       recursive_flag="-maxdepth 1"
    fi

    local files=""
    files=$(find -P "$directory" ${recursive_flag} -regex ".*\.\(hpp\|h\|tpp\|cpp\|c++\|c\|C\)" )

    for f in $files
    do
        if [[ ! -f "$f" ]]; then
            continue
        fi

        $CLANG_FORMAT -i "./$f"
    done
}

format_files()
{
    [[ $# -eq 0 ]] && return 1

    for f in "$*"
    do
        [[ ! -f "$f" ]] && continue

        "$CLANG_FORMAT" -i "$f"
    done
}

usage()
{
which "$CLANG_FORMAT"
cat <<LAB_USAGE

$(basename "$0")  ver. $SCRIPT_VERSION

usage:
   Format C/C++ source-code

   -d    directory(s)
   -R    recursive (include sub directory, used with option -d)
   -f    file
   -c    create format file in current directory
   -C    only create format file in current directory
   -F    list of files (options after this options are ignored)
   -h    Show this help

Example:
  format-code.sh -d <dir1>
 or
  format-code.sh -d <dir1> -d <dir2>
 or
  current directory
      format-code.sh -d .
 or
  directory and sub-directories
      format-code.sh -R -d <dir1>

  format-code.sh -f <file1>

 or
   format-code.sh -F <file1> <file2> <file3>
   Note: this command is can be used with git

LAB_USAGE
}

install_help()
{
cat <<LAB_INSTALL_HELP

  $CLANG_FORMAT is not installed!

    sudo apt-get install $CLANG_FORMAT

LAB_INSTALL_HELP
}

### main ###
[[ $# -eq 0 ]] && usage
$CLANG_FORMAT --version > /dev/null
[[ $? -ne 0 ]] && install_help && exit 1

src_dir=""
src_file=""
flag_files=0
flag_dir=0
recursive_dir=0

FORMAT_FILENAME=$(pwd)/.clang-format

while getopts "d:F:f:cCRhu" opt
do
    case $opt in
        d)
            src_dir="${src_dir} $OPTARG"
            flag_dir=1
            ;;
        f)
            src_file="${src_file} $OPTARG"
            flag_files=1
            ;;
        F)
            src_file="$*"
            flag_files=1
            break
            ;;
        c)
            cp ${DEFAULT_CLANG_FORMAT} ${FORMAT_FILENAME}
            ;;
        C)
            cp ${DEFAULT_CLANG_FORMAT} ${FORMAT_FILENAME}
            exit 0
            ;;
        R)
            recursive_dir=1
            ;;
        h|u)
            usage
            exit 1
            ;;
    esac
done

[[ ! -e ${FORMAT_FILENAME} ]] && cp ${DEFAULT_CLANG_FORMAT} ${FORMAT_FILENAME}
[[ ${flag_files} -ne 0 ]] && format_files ${src_file}
[[ ${flag_dir} -ne 0 ]] && format_files_in_directory "${recursive_dir}" ${src_dir}

