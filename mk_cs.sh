#!/bin/bash
#
#
TMP_CSCOPE_FILE=/tmp/cscope.tmp

die()
{
    echo "$*"
    exit 1
}

which ctags > /dev/null
[[ $? -ne 0 ]] && die "ERR: ctags is not installed"

which cscope > /dev/null
[[ $? -ne 0 ]] && die "ERR: cscope is not installed"

echo > $TMP_CSCOPE_FILE

if [[ $# -eq 0 ]]; then
   find -P . -maxdepth 1 -regex ".*\.\(hpp\|h\|tpp\|cpp\|c++\|c\|C\)" >> $TMP_CSCOPE_FILE 2> /dev/null
else
    for dirName in $*
    do
        if [[ ! -d $dirName ]]; then
            continue
        fi

        echo $dirName
        find -P $dirName -regex ".*\.\(hpp\|h\|tpp\|cpp\|c++\|c\|C\)" >> $TMP_CSCOPE_FILE 2> /dev/null
    done
fi

sort $TMP_CSCOPE_FILE > cscope.files

cscope -b 2> /dev/null

ctags -L cscope.files

