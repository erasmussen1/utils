'''
Minimal clang-rename integration with Vim.

Before installing make sure one of the following is satisfied:

* clang-rename is in your PATH
* `g:clang_tidy_path` in ~/.vimrc points to valid clang-tidy executable
* `binary` in clang-rename.py points to valid to clang-tidy executable

To install, simply put this into your ~/.vimrc

    noremap <F8> :pyf <path-to>/clang-tidy-simple.py<cr>

IMPORTANT NOTE: Before running the tool, make sure you saved the file.

All you have to do now is to place a cursor on a variable/function/class which
you would like to rename and press '<leader>cr'. You will be prompted for a new
name if the cursor points to a valid symbol.

Test of tidy:
  find -name '*.c' | xargs -n 1 clang-tidy -p build

'''

import vim
import subprocess
import sys



tidyOptions="-checks=*"
tidyOptions+=",-readability-implicit-bool-cast"
tidyOptions+=",-google-build-using-namespace"
tidyOptions+=",-google-readability-braces-around-statements"
tidyOptions+=",-cppcoreguidelines-pro-type-union-access"
tidyOptions+=",-cppcoreguidelines-pro-bounds-pointer-arithmetic"
tidyOptions+=",-cppcoreguidelines-pro-type-cstyle-cast"
tidyOptions+=",-cppcoreguidelines-pro-bounds-array-to-pointer-decay"
tidyOptions+=",-llvm-include-order"


def main():
    binary = 'clang-tidy'
    if vim.eval('exists("g:clang_tidy_path")') == "1":
        binary = vim.eval('g:clang_tidy_path')

    if vim.eval('exists("g:clang_compilation_database")') == "0":
        print "Compile db not found"
        return

    compilation_database = vim.eval('g:clang_compilation_database')

    filename = vim.current.buffer.name

    # Call clang-tidy.
    command = [binary, tidyOptions, '-p=' + str(compilation_database), filename]

    print command

    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    stdout, stderr = p.communicate()

    if stderr:
        print stderr

    # Write results from tidy to file
    tidyFilename = "tidy.results"
    f = open(tidyFilename, 'w')
    f.write(stdout)
    f.close()

    # Reload all buffers in Vim.
    vim.command("bufdo edit")

if __name__ == '__main__':
    main()
