#!/bin/bash
#
#

# Just don't forget to link your project against the library by setting -lgtest as
# linker flag and optionally, if you did not write your own test mainroutine, the
# explicit -lgtest_main flag.

die() {
    echo "$@"
    exit 1
}

install_gtest()
{
    if [[ ! -e googletest/ ]]
    then
        git clone https://github.com/google/googletest.git
    fi

    # Building and installing gtest
    pushd googletest/

    git pull

    cmake -DBUILD_SHARED_LIBS=ON .

    make

    sudo make install

    sudo ldconfig

    popd

    # Cleanup
    # [[ -e googletest/ ]] && rm -rf googletest/
}


install_gtest

