" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2001 Jul 18
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" This is the Vundle package, which can be found on GitHub.
"
"   git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
"
" For GitHub repos, you specify plugins using the
" 'user/repository' format
Plugin 'gmarik/vundle'

" We could also add repositories with a ".git" extension
Plugin 'scrooloose/nerdtree.git'

Plugin 'Tagbar'

Plugin 'octol/vim-cpp-enhanced-highlight'
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_experimental_simple_template_highlight = 1

" To get plugins from Vim Scripts, you can reference the plugin
" by name as it appears on the site
Plugin 'Buffergator'

" Added for Python dev
"Plugin 'hynek/vim-python-pep8-indent'
Plugin 'python-mode/python-mode.git'
"Plugin 'davidhalter/jedi-vim'

" Now we can turn our filetype functionality back on
filetype plugin indent on

" Pylint configuration file
let g:pymode_lint_config = '$HOME/pylint.rc'
"let g:pymode_options = 1
let g:pymode_options_max_line_length = 120

set path+=**

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set autoindent		" always set autoindenting on
set nobackup		" do not keep a backup file, use versions instead

set history=50		" keep 50 lines of command line history
set ruler		    " show the cursor position all the time
set showcmd		    " display incomplete commands
set incsearch		" do incremental searching
set tabstop=4       " tab width = 4 spaces
set expandtab       " don't put tab characters in the file

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" Make p in Visual mode replace the selected text with the "" register.
vnoremap p <Esc>:let current_reg = @"<CR>gvs<C-R>=current_reg<CR><Esc>

" This is an alternative that also works in block mode, but the deleted
" text is lost and it only works for putting the current register.
"vnoremap p "_dp

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch

  set t_Co=256
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif

endif " has("autocmd")


" Set options and add mapping such that Vim behaves a lot like MS-Windows
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2002 Mar 05

" set the 'cpoptions' to its Vim default
if 1	" only do this when compiled with expression evaluation
  let s:save_cpo = &cpoptions
endif
set cpo&vim

" set 'selection', 'selectmode', 'mousemodel' and 'keymodel' for MS-Windows
behave mswin

" backspace and cursor keys wrap to previous/next line
set backspace=2 whichwrap+=<,>,[,]

" backspace in Visual mode deletes selection
vnoremap <BS> d

" CTRL-X and SHIFT-Del are Cut
vnoremap <C-X>   "+x
vnoremap <S-Del> "+x

" CTRL-C and CTRL-Insert are Copy
vnoremap <C-C>      "+y
vnoremap <C-Insert> "+y

" CTRL-V and SHIFT-Insert are Paste
map <C-V>      "+gP
map <S-Insert> "+gP

cmap <C-V>      <C-R>+
cmap <S-Insert> <C-R>+

" Pasting blockwise and linewise selections is not possible in Insert and
" Visual mode without the +virtualedit feature.  They are pasted as if they
" were characterwise instead.
if has("virtualedit")
  nnoremap <silent> <SID>Paste :call <SID>Paste()<CR>
  func! <SID>Paste()
    let ove = &ve
    set ve=all
    normal `^"+gPi
    let &ve = ove
  endfunc
  imap <C-V>		<Esc><SID>Pastegi
  vmap <C-V>		"-c<Esc><SID>Paste
else
  nnoremap <silent> <SID>Paste "=@+.'xy'<CR>gPFx"_2x
  imap <C-V>		x<Esc><SID>Paste"_s
  vmap <C-V>		"-c<Esc>gix<Esc><SID>Paste"_x
endif

imap <S-Insert>    <C-V>
vmap <S-Insert>    <C-V>

" Use CTRL-Q to do what CTRL-V used to do
noremap <C-Q>		<C-V>

" Use CTRL-S for saving, also in Insert mode
noremap <C-S>		:update<CR>
vnoremap <C-S>		<C-C>:update<CR>
inoremap <C-S>		<C-O>:update<CR>

" For CTRL-V to work autoselect must be off.
" On Unix we have two selections, autoselect can be used.
if !has("unix")
  set guioptions-=a
  colorscheme darkblue
endif

" CTRL-Z is Undo; not in cmdline though
noremap <C-Z> u
inoremap <C-Z> <C-O>u

" CTRL-Y is Redo (although not repeat); not in cmdline though
noremap <C-Y> <C-R>
inoremap <C-Y> <C-O><C-R>

" CTRL-A is Select all
noremap <C-A> gggH<C-O>G
inoremap <C-A> <C-O>gg<C-O>gH<C-O>G
cnoremap <C-A> <C-C>gggH<C-O>G

" CTRL-Tab is Next window
noremap <C-Tab> <C-W>w
inoremap <C-Tab> <C-O><C-W>w
cnoremap <C-Tab> <C-C><C-W>w

" restore 'cpoptions'
set cpo&
if 1
  let &cpoptions = s:save_cpo
  unlet s:save_cpo
endif

"My settings "
autocmd BufWritePre * :%s/\s\+$//e

set hls
set spell
set expandtab
set tabstop=4
set shiftwidth=4  "Indent in visual mode "
set cindent
set smartindent
set nobackup		" keep a backup file
set autoindent

if has('cscope')
  " use both cscope and ctag for 'ctrl-]', ':ta', and 'vim -t'
  set cscopetag cscopeverbose

  if has('quickfix')
    set cscopequickfix=s-,c-,d-,i-,t-,e-
  endif

  cnoreabbrev csa cs add
  cnoreabbrev csf cs find
  cnoreabbrev csk cs kill
  cnoreabbrev csr cs reset
  cnoreabbrev css cs show
  cnoreabbrev csh cs help

  command -nargs=0 Cscope cs add $VIMSRC/src/cscope.out $VIMSRC/src
endif

" Disable annoying beeping
set noerrorbells
set vb t_vb=

command! MakeTags !ctags -R .

" let g:ycm_global_ycm_extra_conf = "~/.ycm_extra_conf.py"
" let g:ycm_key_list_select_completion=[]
" let g:ycm_key_list_previous_completion=[]

" set F1 to toggle to previous buffer
nnoremap <F1> :bp<cr>

" set F2 to toggle to next buffer
nnoremap <F2> :bn<cr>

" set F3 to toggle and untogle Nerdtree
autocmd VimEnter * nmap <F3> :NERDTreeToggle<CR>
autocmd VimEnter * imap <F3> <Esc>:NERDTreeToggle<CR>a
let NERDTreeQuitOnOpen=0
let NERDTreeWinSize=35
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

autocmd VimEnter * nmap <C-F3> :TagbarToggle<CR>
autocmd VimEnter * nmap <C-F3> <Esc> :TagbarToggle<CR>

" Settings for vim-clang
let g:clang_auto = 1
let g:clang_c_options = '-std=c11'
let g:clang_cpp_options = '-std=c++17 -stdlib=libc++'

let g:clang_tidy_path = "/usr/bin/clang-tidy"
let g:clang_rename_path = "/usr/bin/clang-rename"
let g:clang_format_path = "/usr/bin/clang-format"

let g:clang_compilation_database = "./build"

" clang-rename
if has('python')
  noremap <F4> :pyf ~/bin/clang-rename.py2<cr>
elseif has('python3')
  noremap <F4> :pyf ~/bin/clang-rename.py<cr>
endif

" For Python dev
" autocmd VimEnter * nmap <F5> :!python3 %<cr>
" autocmd VimEnter * imap <F5> <Esc>:!python3 %<cr>a
map <F5> :make -j 8 -C build

" clang-tidy
noremap <F8> :pyf ~/bin/clang-tidy-simple.py<cr>
" noremap <F8> :!clang-tidy -p build/ %<cr>

noremap <F9> :TagbarToggle<cr>

" clang-format
if has('python')
  map <C-K> :pyf ~/bin/clang-format2.py<cr>
  imap <C-K> <c-o>:pyf ~/bin/clang-format2.py<cr>
elseif has('python3')
  map <C-K> :py3f ~/bin/clang-format.py<cr>
  imap <C-K> <c-o>:py3f ~/bin/clang-format.py<cr>
endif

" Custom GREP search results, Third level of Ctrl-p-p-p.
" opens search results in a window w/ links and highlight the matches, for the word under the cursor
"#command! -nargs=+ CGrep :execute 'silent grep -I -r -n --include=\*.{c,cc,cpp,cxx,h,hpp,hxx} . -e <args>' | copen | execute ':redraw!'
"#command! -nargs=+ CFind :execute  'silent grep -Ril    --include=\*.{c,cc,cpp,cxx,h,hpp,hxx} . -e <args>' | copen | execute ':redraw!'
"#command! -nargs=+ Grep :execute  'silent grep -I -r -n --include=\*.{c,cc,cpp,cxx,h,hpp,hxx,py,php,txt,cmake} . -e <args>' | copen | execute ':redraw!'
"#command! -nargs=+ Find :execute  'silent grep -Ril     --include=\*.{c,cc,cpp,cxx,h,hpp,hxx,py,php,txt,cmake} . -e <args>' | copen | execute ':redraw!'
"#nmap <C-p> :CGrep <c-r>=expand("<cword>")<cr><cr>

"======================================================
" Function runs git blame on file in current buffer and
" puts output into a new window
" move to current line in git output
" (can't edit output)
" https://stackoverflow.com/questions/33051496/custom-script-for-git-blame-from-vim
"======================================================
command! -nargs=* Blame call s:GitBlame()

function! s:GitBlame()
   let cmdline = "git blame -w " . bufname("%")
   let nline = line(".") + 1
   botright new
   setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile nowrap
   execute "$read !" . cmdline
   setlocal nomodifiable
   execute "normal " . nline . "gg"
   execute "set filetype=cpp"
endfunction


"Clear the colors for any items that you don't like
hi clear StatusLine
hi clear StatusLineNC

"Set up your new & improved colors
hi StatusLine guifg=black guibg=white
hi StatusLineNC guifg=LightCyan guibg=blue gui=bold

set encoding=utf-8

