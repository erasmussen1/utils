#!/bin/bash
#
#

die() {
    echo "$*"
    exit 1
}

which clang-tidy
[[ $? -ne 0 ]] && die "ERR: clang-tidy is not install on your system"

filename="$1"
[[ ! -e "$filename" ]] && die "ERR: files does not exist $filename "

for d in "build" "build/target" "build/host" "build_blade_debug" "build_host_debug"
do
  [[ ! -d ${d} ]] && continue
  [[ ! -e ${d}/compile_commands.json ]] && continue
  dir="${d}"
done

[[ -z $dir ]] && die "ERR: can not find generated compiled json file"

CHECK_EXCLUDE="-readability-implicit-bool-cast,-google-build-using-namespace,-google-readability-braces-around-statements"
CHECK_EXCLUDE="${CHECK_EXCLUDE},-cppcoreguidelines-pro-type-union-access"
CHECK_EXCLUDE="${CHECK_EXCLUDE},-cppcoreguidelines-pro-bounds-pointer-arithmetic"
CHECK_EXCLUDE="${CHECK_EXCLUDE},-cppcoreguidelines-pro-type-cstyle-cast"
CHECK_EXCLUDE="${CHECK_EXCLUDE},-cppcoreguidelines-pro-bounds-array-to-pointer-decay"
CHECK_EXCLUDE="${CHECK_EXCLUDE},-llvm-include-order"

clang-tidy -p "$dir" -checks=*,$CHECK_EXCLUDE "$filename"

# clang-tidy -fix -p "$dir" -checks=*,$CHECK_EXCLUDE "$filename"
# clang-tidy -p "$dir" -checks=-*,clang-analyzer-*,-clang-analyzer-cplusplus* "$filename"
# clang-tidy -fix -p "$dir" -checks=-*,modernize-* "$filename"

